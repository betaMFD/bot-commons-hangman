
## Personal Project

This project is unlikely to plug into another bot without setup so it's for me only.

You can use it as a reference though for your own code. 

Feel free to steal the json if you want. I curated it from lists around the internet. If you modify the json for your own project, please do a pull request to add more options for this project! Thank you!


## Hooking It Up
Add this to the bot.mjs file to make the scripts work

```
import {hangmanScript, hangmanListen} from 'bot-script-hangman/src/hangman.mjs';
// Add the hangman script directly. Both are required.
client.scripts.set('hangman', hangmanScript);
client.scripts.set('hangman_listen', hangmanListen);
```
