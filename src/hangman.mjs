// Description:
//    runs a game of hangman

import { EmbedBuilder } from 'discord.js';
import { readFileSync } from 'fs';

const rawdata = readFileSync('node_modules/bot-commons-hangman/resources/hangman.json');
const hangman_data = JSON.parse(rawdata);

let hangman_game = false;
let game_topic = '';
let game_phrase = '';
let game_won = false;
let hangman_channel = false;
const guessed_letters = [];
const bad_letters = [];
const good_letters = [];

export const hangmanScript = {
  regex: /hangman/i,
  tag_bot: true,
  execute: function(message) {
    if (hangman_game) {
      // game is already running.
      message.reply("There's already a game running.");
    } else {
      hangman_game = true;
      hangman_channel = message.channel.id;
      // game started
      getPhrase();
      const channel = message.channel;
      channel.send({embeds: getEmbed('Start! Topic is ' + game_topic)});
    }
  }
}


/**
 * Runs on every received message
 * Checks to see if the message is for the game
 * Checks guesses and letters against the game and responds appropriately
 */
export const hangmanListen = {
  regex: /.*$/,
  tag_bot: false,
  execute: function(message) {
    if (hangman_game // game is running
      && message.channel.id === hangman_channel // limit the channel
      && !message.author.bot // not a bot
    ) {
      const channel = message.channel;
      // is it a letter or a match?
      if (message.content.toLowerCase() === game_phrase.toLowerCase()) {
        game_won = true;
        channel.send({embeds: getEmbed('You win!')});
        resetGame();
      } else if (message.content.match(/^[a-z]$/i)) {
        // this is a letter guess
        let letter = message.content.toLowerCase();
        if (guessed_letters.includes(letter)) {
          channel.send({embeds: getEmbed('Letter already guessed: ' + letter)});
        } else {
          // letter hasn't been guessed already.
          guessed_letters.push(letter);
          // Is it a good guess or a bad guess?
          let regex = new RegExp(letter, 'i');
          if (game_phrase.match(regex)) {
            // good guess!
            good_letters.push(letter);
            channel.send({embeds: getEmbed('Good guess!')});
          } else {
            // bad guess =(
            bad_letters.push(letter);
            // only 6 guesses allowed, have we hit that?
            if (bad_letters.length >= 6) {
              channel.send({embeds: getEmbed('You lose!')});
              resetGame();
            } else {
              channel.send({embeds: getEmbed('Bad guess =(')});
            }
          }
        }
      }
    }
  }
}


function getPhrase(topic) {
  if (!topic) {
    // choose one randomly
    let topic_keys = Object.keys(hangman_data);
    topic = topic_keys[Math.floor(Math.random() * topic_keys.length)];
  }
  game_topic = topic;
  let keys = Object.keys(hangman_data[topic]);
  game_phrase = keys[Math.floor(Math.random() * keys.length)];
}

function getEmbed(title) {
  let description = buildHangman();
  let progress = buildProgress();
  let url = false;

  if (bad_letters.length >= 6) {
    // You've lost! oh no
    title = 'GAME OVER!';
    description += '\n**YOU LOST!**\n';
    description += '`' + game_phrase + '`\n';
    url = hangman_data[game_topic][game_phrase];
  } else if (game_won) {
    title = 'YOU WON!!';
    description += '\n**Good Job!**\n';
    description += '`' + game_phrase + '`\n';
    url = hangman_data[game_topic][game_phrase];
  } else {
    description += '\n**Guesses**\n';
    description += '`' + progress + '`\n';
  }
  description += bad_letters.join(' ');

  const embed = new EmbedBuilder()
    .setColor('#0099ff')
    .setTitle(title)
    .setDescription(description)
    ;
  if (url) {
    embed.setImage(url);
  }
  if (game_won) {
    resetGame();
  }
  return [embed];
}

function buildProgress() {
  let progress = '';
  let fail = false;
  // loop through the letters of the game phrase
  for (let i in game_phrase) {
    let cur_letter = game_phrase[i];
    // is this a valid letter?
    if (cur_letter.match(/[a-z]/i)) {
      // valid letter for guessing
      // have we matched it?
      if (good_letters.includes(cur_letter.toLowerCase())) {
        progress += cur_letter;
      } else {
        fail = true;
        progress += '_';
      }
    } else {
      // not a valid letter
      progress += cur_letter;
    }
    progress += ' ';
  }
  if (!fail) {
    game_won = true;
  }
  return progress;
}

function buildHangman() {
  let hangman = `
⠀ ┌───┐\n`;

  switch (bad_letters.length) {
    case 0:
      hangman += '⠀  ┃\n';
      hangman += '⠀  ┃\n';
      hangman += '⠀  ┃\n';
      break;
    case 1:
      hangman += '⠀  ┃⠀⠀⠀ O\n';
      hangman += '⠀  ┃\n';
      hangman += '⠀  ┃\n';
      break;
    case 2:
      hangman += '⠀  ┃⠀⠀⠀ O\n';
      hangman += '⠀  ┃⠀⠀⠀/\n';
      hangman += '⠀  ┃\n';
      break;
    case 3:
      hangman += '⠀  ┃⠀⠀⠀ O\n';
      hangman += '⠀  ┃⠀⠀⠀/|\n';
      hangman += '⠀  ┃\n';
      break;
    case 4:
      hangman += '⠀  ┃⠀⠀⠀ O\n';
      hangman += '⠀  ┃⠀⠀⠀/|\\\n';
      hangman += '⠀  ┃\n';
      break;
    case 5:
      hangman += '⠀  ┃⠀⠀⠀ O\n';
      hangman += '⠀  ┃⠀⠀⠀/|\\\n';
      hangman += '⠀  ┃⠀⠀⠀/\n';
      break;
    case 6:
      hangman += '⠀  ┃⠀⠀⠀ O\n';
      hangman += '⠀  ┃⠀⠀⠀/|\\\n';
      hangman += '⠀  ┃⠀⠀⠀/ \\\n';
      break;
  }
  return hangman + '⠀/\\\n';
}

/**
 * Game over! Reset all the game data to default
 */
function resetGame() {
  hangman_game = false;
  game_topic = '';
  game_phrase = '';
  guessed_letters.length = 0;
  bad_letters.length = 0;
  good_letters.length = 0;
  game_won = false;
  hangman_channel = false;
}
